import {TicTacToePresenter} from "./tic-tac-toe/tic_tac_toe_presenter.js";

class Starter {
    static #presenter;

    static start() {
        if (!this.#presenter) {
            this.#presenter = new TicTacToePresenter();
        }
    }
}

Starter.start();
