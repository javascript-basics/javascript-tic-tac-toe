export function getElementByClassName(className) {
    return (document.getElementsByClassName(className) || document.createDocumentFragment().children).item(0);
}

export function* elementGenerator(className) {
    const buttons = document.getElementsByClassName(className);
    for (let i = 0; i < buttons.length; i++) {
        yield buttons.item(i);
    }
}
