import {elementGenerator, getElementByClassName} from "./helpers.js";

const MODAL_TITLE_CLASS = 'modal-title';
const MODAL_BODY_CLASS = 'modal-body';
const MODAL_WINDOW_CLASS = 'modal-window';
const MODAL_BTN_CLASS = 'modal-btn';

const CLOSING_TIMEOUT = 1000;
const MODAL_WINDOW_BACKGROUND = 'rgba(0, 0, 0, 0.4)';

export class ModalView {
    #modalTitleElement = Object.create(HTMLElement.prototype, {});
    #modalBodyElement = Object.create(HTMLElement.prototype, {});
    #modalWindowElement = Object.create(HTMLElement.prototype, {});

    get modalButtons() {
        return elementGenerator(MODAL_BTN_CLASS);
    }

    constructor() {
        this.#init();
    }

    #init() {
        this.#modalTitleElement = getElementByClassName(MODAL_TITLE_CLASS);
        this.#modalBodyElement = getElementByClassName(MODAL_BODY_CLASS);
        this.#modalWindowElement = getElementByClassName(MODAL_WINDOW_CLASS);
    }

    openModal() {
        this.#modalWindowElement.style.display = 'flex';
        this.#modalWindowElement.style.background = MODAL_WINDOW_BACKGROUND;
        this.#modalBodyElement.style.opacity = '1';
    }

    closeModal() {
        this.#animateClosing();
        setTimeout(this.#hideModal.bind(this), CLOSING_TIMEOUT);
    }

    #animateClosing() {
        this.#modalWindowElement.style.background = null;
        this.#modalBodyElement.style.opacity = '0';
    }

    #hideModal() {
        this.#modalWindowElement.style.display = 'none';
    }

    updateTitle(value) {
        this.#modalTitleElement.innerText = value;
    }
}
