import {ModalView} from "./modal_view.js";
import {TicTacToeView} from "./tic_tac_toe_view.js";
import {TicTacToeService} from "./tic_tac_toe_service.js";

export class TicTacToePresenter {
    #modalView = new ModalView();
    #ticTacToeView = new TicTacToeView();
    #ticTacToeService = new TicTacToeService();

    constructor() {
        this.#init()
    }

    #init() {
        this.#initModalButtonsListeners();
        this.#initButtonsListeners();
    }

    #initModalButtonsListeners() {
        const buttons = this.#modalView.modalButtons;

        for (let button of buttons) {
            (button || new HTMLElement()).addEventListener('click', this.#handleModalButtonClick.bind(this));
        }
    }

    #handleModalButtonClick(pointerEvent) {
        const playerChar = ((pointerEvent || new Event('click')).target || new HTMLElement()).innerText;
        this.#ticTacToeService = new TicTacToeService(playerChar);

        this.#modalView.closeModal();
        this.#ticTacToeView.resetButtons();

        this.#updateBoard();
    }

    #initButtonsListeners() {
        const buttons = this.#ticTacToeView.buttons;
        const digitRegExp = new RegExp('\\d+', 'g');

        for (let button of buttons) {
            const [row, column] = [...(button || new HTMLElement()).id.match(digitRegExp)];
            (button || new HTMLElement()).addEventListener('click', this.#handleButtonClick.bind(this, row, column));
        }
    }

    #handleButtonClick(row, column) {
        if (!this.#ticTacToeService.checkIfAvailable(row, column)) {
            return;
        }

        this.#ticTacToeService.setPlayerMove(row, column);
        this.#ticTacToeService.calculateComputerMove();
        this.#updateBoard();

        const status = this.#ticTacToeService.status;

        if (status) {
            this.#modalView.updateTitle(status);
            this.#modalView.openModal();
        }
    }

    #updateBoard() {
        const board = this.#ticTacToeService.board || [];

        for (let row = 0; row < board.length; row++) {
            for (let column = 0; column < board.length; column++) {
                const value = board[row][column];

                if (!value) {
                    continue;
                }

                const id = `${row}-${column}`;
                this.#ticTacToeView.updateButtonContent(id, value);
            }
        }
    }
}
