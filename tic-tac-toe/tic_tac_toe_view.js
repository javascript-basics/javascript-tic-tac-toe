import {elementGenerator} from "./helpers.js";

const BUTTONS_CLASS = 'field-btn';
const PASSED_BUTTON_CLASS = 'field-btn-with-value';

export class TicTacToeView {
    get buttons() {
        return elementGenerator(BUTTONS_CLASS);
    }

    constructor() {
        this.#init();
    }

    #init() {
        const buttons = this.buttons;

        let row = 0;
        let column = 0;

        for (let button of buttons) {
            if (column === 3) {
                row++;
                column = 0;
            }

            (button || new HTMLElement()).id = `${row}-${column}`;
            (button || new HTMLElement()).classList.remove(PASSED_BUTTON_CLASS);

            column++;
        }
    }

    resetButtons() {
        const buttons = this.buttons;

        for (let button of buttons) {
            (button || new HTMLElement()).innerText = '';
            (button || new HTMLElement()).classList.remove(PASSED_BUTTON_CLASS);
        }
    }

    updateButtonContent(id, content) {
        const button = document.getElementById(id) || new HTMLElement();
        button.innerText = content;
        button.classList.add(PASSED_BUTTON_CLASS);
    }
}
