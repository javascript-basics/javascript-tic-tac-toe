import {TicTacToe} from "./tic_tac_toe.js";

const PLAYER_WIN_STATUS = 'Player Wins';
const COMPUTER_WIN_STATUS = 'Computer Wins';
const DRAW_STATUS = 'Draw';

export class TicTacToeService {
    #ticTacToe = new TicTacToe();

    get board() {
        return this.#ticTacToe.board;
    }

    get status() {
        if (this.#ticTacToe.playerWinCondition) {
            return PLAYER_WIN_STATUS;
        }

        if (this.#ticTacToe.computerWinCondition) {
            return COMPUTER_WIN_STATUS;
        }

        if (this.#ticTacToe.drawCondition) {
            return DRAW_STATUS;
        }
    }

    constructor(playerChar) {
        this.#init(playerChar);
    }

    #init(playerChar) {
        this.#ticTacToe = new TicTacToe(playerChar);
    }

    checkIfAvailable(row, column) {
        return this.#ticTacToe.checkIfAvailable(row, column);
    }

    setPlayerMove(row, column) {
        this.#ticTacToe.setPlayerMove(row, column);
    }

    calculateComputerMove() {
        this.#ticTacToe.calculateComputerMove();
    }
}
