const CROSS_CHAR = 'X';
const ZERO_CHAR = '0';

const EMPTY_VALUE = '';
const EMPTY_BOARD = [
    [EMPTY_VALUE, EMPTY_VALUE, EMPTY_VALUE],
    [EMPTY_VALUE, EMPTY_VALUE, EMPTY_VALUE],
    [EMPTY_VALUE, EMPTY_VALUE, EMPTY_VALUE]
];

const PLAYER_WIN_SCORE = {score: -1};
const DRAW_SCORE = {score: 0};
const COMPUTER_WIN_SCORE = {score: 1};

export class TicTacToe {
    #playerChar;
    #computerChar;
    #board = [];

    get board() {
        return this.#board;
    }

    get playerWinCondition() {
        return TicTacToe.#checkWinningCondition(this.#playerChar, this.#board);
    }

    get computerWinCondition() {
        return TicTacToe.#checkWinningCondition(this.#computerChar, this.#board);
    }

    get drawCondition() {
        return !(TicTacToe.#getAvailableSteps(this.#board) || []).length;
    }

    constructor(playerChar) {
        this.#init(playerChar);
    }

    #init(playerChar) {
        this.#playerChar = playerChar;
        this.#board = JSON.parse(JSON.stringify(EMPTY_BOARD));

        this.#initComputerChar();

        if (playerChar === ZERO_CHAR) {
            this.calculateComputerMove();
        }
    }

    #initComputerChar() {
        this.#computerChar = this.#playerChar === CROSS_CHAR ? ZERO_CHAR : CROSS_CHAR;
    }

    calculateComputerMove() {
        const bestMove = this.#getBestMove(this.#computerChar, this.#board) || {index: null};

        if (!bestMove.index) {
            return;
        }

        const {row, column} = bestMove.index;
        this.#board[row][column] = this.#computerChar;
    }

    // MiniMax function
    #getBestMove(char, board) {
        const availableSteps = TicTacToe.#getAvailableSteps(board) || [];

        if (TicTacToe.#checkWinningCondition(this.#playerChar, board)) {
            return PLAYER_WIN_SCORE;
        } else if (TicTacToe.#checkWinningCondition(this.#computerChar, board)) {
            return COMPUTER_WIN_SCORE;
        } else if (!availableSteps.length) {
            return DRAW_SCORE;
        }

        const moves = [];

        availableSteps.forEach(availableStep => {
            const move = {
                index: availableStep
            }

            const {row, column} = availableStep;
            board[row][column] = char;

            move.score = this.#getBestMove(char === this.#computerChar ? this.#playerChar : this.#computerChar,
                board).score;

            board[row][column] = EMPTY_VALUE;
            moves.push(move);
        });

        const mathFunction = char === this.#computerChar ? 'max' : 'min';
        const scores = moves.map(move => move.score);

        const bestScores = moves.filter(move => move.score === Math[mathFunction](...scores));
        const randomIndex = Math.floor(Math.random() * (bestScores.length - 1));

        return bestScores[randomIndex];
    }

    static #getAvailableSteps(board) {
        const availableSteps = [];

        for (let row = 0; row < board.length; row++) {
            for (let column = 0; column < board.length; column++) {
                if (board[row][column] === EMPTY_VALUE) {
                    availableSteps.push({
                        row,
                        column
                    })
                }
            }
        }

        return availableSteps;
    }

    static #checkWinningCondition(char, board) {
        const principalDiagonalWinningCondition = board[0][0] === char && board[1][1] === char
            && board[2][2] === char;

        if (principalDiagonalWinningCondition) {
            return true;
        }

        const secondaryDiagonalWinningCondition = board[2][0] === char && board[1][1] === char
            && board[0][2] === char;

        if (secondaryDiagonalWinningCondition) {
            return true;
        }

        for (let i = 0; i < board.length; i++) {
            const rowWinningCondition = board[0][i] === char && board[1][i] === char
                && board[2][i] === char;

            if (rowWinningCondition) {

                return true;
            }

            const columnWinningCondition = board[i][0] === char && board[i][1] === char
                && board[i][2] === char;

            if (columnWinningCondition) {
                return true;
            }
        }
    }

    checkIfAvailable(row, column) {
        return this.#board[row][column] === EMPTY_VALUE;
    }

    setPlayerMove(row, column) {
        this.#board[row][column] = this.#playerChar;
    }
}
